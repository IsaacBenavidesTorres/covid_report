from django.test import TestCase, TransactionTestCase
from apps.reports.models import Country, Register
from django.db.utils import IntegrityError


class TestCountry(TestCase):
    def test_country_name(self):
        country = Country.objects.create(country='Spain')
        self.assertEqual(country.country, 'Spain')

    def country_name_is_unique(self):
        Country.objects.create(country='Spain')
        with self.assertRaises(IntegrityError):
            Country.objects.create(country='Spain')
