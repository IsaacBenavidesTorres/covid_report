from django.contrib import admin
from .models import Country, Register

admin.site.register(Country)
admin.site.register(Register)
