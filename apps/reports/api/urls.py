from django.urls import path
from .views import RegisterEditAPIView, RegistersListApiView, RegisterDeleteAPIView


urlpatterns = [
    path('registers/', RegistersListApiView.as_view(),
         name='register-list'),
    path('register/<int:pk>/', RegistersListApiView.as_view(),
         name='register'),
    path('update/<int:pk>/', RegisterEditAPIView.as_view(), name='registers-list'),
    path('delete/<int:pk>/', RegisterDeleteAPIView.as_view()),
]
